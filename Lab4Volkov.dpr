program Lab4Volkov;

uses
  ApplicationManager in 'ApplicationManager.pas',
  MRP in 'MRP.pas',
  Workspace in 'Workspace.pas',
  ReportWindow in 'ReportWindow.pas' {ReportView},
  MainWindow in 'MainWindow.pas' {MainWnd},
  MaterialEditForm in 'MaterialEditForm.pas' {MaterialEditWnd},
  MaterialsForm in 'MaterialsForm.pas' {MaterialsWnd},
  WorkEditForm in 'WorkEditForm.pas' {WorkEditWnd},
  EditMaterialReqForm in 'EditMaterialReqForm.pas' {EditMaterialReqWnd};

var
    AppManager: TApplicationManager;

  begin
     AppManager := TApplicationManager.Create();
     AppManager.run();
  end.

