unit MaterialsForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Workspace, Generics.Collections, MaterialEditForm,
  Spin;

type
  TMaterialCommands = (CommandAdd, CommandRemove, CommandEdit);

  TMaterialCommand = procedure(material:TMaterialInfo; Command:TMaterialCommands) of object;

  TMaterialsWnd = class(TForm)
    ListBox1: TListBox;
    ButtonAdd: TButton;
    ButtonRemove: TButton;
    ButtonEdit: TButton;
    ButtonOk: TButton;
    procedure ButtonAddClick(Sender: TObject);
    procedure ButtonRemoveClick(Sender: TObject);
    procedure ButtonEditClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
  private
    mMaterials: TList<TMaterialInfo>;
  public
    procedure setMaterials(materials: TList<TMaterialInfo>);
  end;

var
  MaterialsWnd: TMaterialsWnd;

implementation

{$R *.dfm}

procedure TMaterialsWnd.ButtonAddClick(Sender: TObject);
var mat: TMaterialInfo;
  editDlg: TMaterialEditWnd;
begin
    RegisterClass(TEdit);
    RegisterClass(TSpinEdit);
    editDlg:= TMaterialEditWnd.Create(self);
    mat:= TMaterialInfo.Create();
    editDlg.setMaterial(mat);
    if (editDlg.showModal() = mrOk) then begin
      mMaterials.Add(mat);
      setMaterials(mMaterials);
    end
    else mat.Free();
end;

procedure TMaterialsWnd.ButtonEditClick(Sender: TObject);
var mat, matCopy: TMaterialInfo;
  editDlg: TMaterialEditWnd;
begin
  if ListBox1.ItemIndex >= 0 then begin
    editDlg := TMaterialEditWnd.Create(self);
    mat:= ListBox1.Items.Objects[ListBox1.ItemIndex] as TMaterialInfo;

    editDlg.setMaterial(mat);
    editDlg.showModal();
    setMaterials(mMaterials);
  end
  else MessageDlg('Select material, please.', mtInformation, [mbOK], 0);
end;

procedure TMaterialsWnd.ButtonOkClick(Sender: TObject);
begin
  Close();
end;

procedure TMaterialsWnd.ButtonRemoveClick(Sender: TObject);
var mat: TMaterialInfo;
begin
  if ListBox1.ItemIndex >= 0 then begin
    if MessageDlg('Really?',
            mtConfirmation, [mbYes, mbNo], 0) = mrYes  then begin
       mat:= ListBox1.Items.Objects[ListBox1.ItemIndex] as TMaterialInfo;
       mMaterials.Remove(mat);
       mat.Free();
       setMaterials(mMaterials);
    end;
  end
  else MessageDlg('Select material, please.', mtInformation, [mbOK], 0);
end;

procedure TMaterialsWnd.setMaterials(materials: TList<TMaterialInfo>);
var item: TMaterialInfo;
begin
  ListBox1.Clear();
  for item in materials do begin
    ListBox1.AddItem(item.mDisplayName, item);
  end;
  ListBox1.ItemIndex := 0;
  mMaterials := materials;
end;

end.
