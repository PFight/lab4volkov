unit MrpTest;

interface

uses

 TestFrameWork, Workspace, MRP, Generics.Collections, Dialogs;

type

 TMrpTests = class(TTestCase)

 protected
    mWorkspace: TWorkspace;
    mMrp: TMrp;

    procedure SetUp; override;
    procedure TearDown; override;
 published
   procedure NothingToDoTest;
   procedure ReserveStockTest;
   procedure OneWorkOneMaterialTest;
   procedure DeliverySizeIncrementTest;
   procedure TwoParallelWorksOnOrder;
   procedure TwoNotParallelWorksTwoOrders;
   procedure TwoWorksTwoMaterials;
 end;

 function equalTime(time1, time2: TDateTime):Boolean;

implementation

function equalTime(time1, time2: TDateTime):Boolean;
begin
  // Double comparision
  equalTime:= abs(time1 - time2) < 0.00001;
end;

procedure TMrpTests.SetUp;
begin
  inherited;
  mWorkspace := TWorkspace.Create();
  mMrp := TMrp.Create(mWorkspace)
end;

procedure TMrpTests.TearDown;
begin
  inherited;
  mWorkspace.Free();
  mMrp.Free();
end;

procedure TMrpTests.NothingToDoTest;
begin
  // Should not crash
  mMrp.formOrders();
end;

procedure TMrpTests.ReserveStockTest;
var order : TMaterialOrderItem;
 mat: TMaterialInfo;
 work: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
 item: TWorkspaceItem;
 count: integer;
begin
  inherited;

  mat := TMaterialInfo.Create();
  mat.mName := 'A';
  mat.mDisplayName := 'Material A';
  mat.mDeliveryTime := 4;
  mat.mMinDeliverySize := 7;
  mat.mDeliveryInc := 2;
  mat.mReserveStock := 3;
  mWorkspace.addMaterial(mat);

  mat := TMaterialInfo.Create();
  mat.mName := 'B';
  mat.mDisplayName := 'Material B';
  mat.mDeliveryTime := 7;
  mat.mMinDeliverySize := 3;
  mat.mDeliveryInc := 1;
  mat.mReserveStock := 0;
  mWorkspace.addMaterial(mat);

  // Work do not requires materials
  reqMatList := TList<TMaterialRequirement>.Create();
  work := TManufactureWorkItem.Create('work', 0, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work);

  mMrp.formOrders();

  // Material A has reserve stock 3, so
  // result of the test should be one material order
  // of material A with ammount 7 (min delivery size of A)

  Check(mWorkspace.getItems().Count = 2, 'Invalid item count (should be 2 - work item + one order)');

  order:= nil;
  count := 0;
  for item in mWorkspace.getItems() do begin
     if item.getType() = MaterialOrder then begin
       order := item as TMaterialOrderItem;
       inc(count);
     end;
  end;
  Check(order <> nil, 'There is no material order');
  Check(count = 1, 'There is more then one order');
  Check(order.getMaterialName() = 'A', 'Invalid material');
  Check(order.getMaterialAmount() = mWorkspace.getMaterialInfo('A').mMinDeliverySize, 'Invalid amount');
end;

procedure TMrpTests.OneWorkOneMaterialTest;
var order : TMaterialOrderItem;
 mat: TMaterialInfo;
 work: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
 item: TWorkspaceItem;
 count: integer;
begin
  inherited;

  mat := TMaterialInfo.Create();
  mat.mName := 'A';
  mat.mDisplayName := 'Material A';
  mat.mDeliveryTime := 4;
  mat.mMinDeliverySize := 7;
  mat.mDeliveryInc := 2;
  mat.mReserveStock := 0;
  mWorkspace.addMaterial(mat);

  // Add work, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat;
  reqmat.mAmout := 3;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work := TManufactureWorkItem.Create('work', 42, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work);

  mMrp.formOrders();

  // Result of the test should be one material order
  // of material A with ammount 7 (min delivery size of A)
  // with finish time 42 (work start time)

  Check(mWorkspace.getItems().Count = 2, 'Invalid item count (should be 2 - work item + one order)');

  order:= nil;
  count := 0;
  for item in mWorkspace.getItems() do begin
     if item.getType() = MaterialOrder then begin
       order := item as TMaterialOrderItem;
       inc(count);
     end;
  end;
  Check(order <> nil, 'There is no material order');
  Check(count = 1, 'There is more then one order');
  Check(order.getMaterialName() = 'A', 'Invalid material');
  Check(order.getMaterialAmount() = mWorkspace.getMaterialInfo('A').mMinDeliverySize, 'Invalid amount');
  Check(order.getFinishTime() = work.getStartTime()-1, 'Invalid finish time');
  Check(order.getStartTime() = (work.getStartTime() - mat.mDeliveryTime), 'Invalid start time');

end;

procedure TMrpTests.DeliverySizeIncrementTest;
var order : TMaterialOrderItem;
 mat: TMaterialInfo;
 work: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
 item: TWorkspaceItem;
 count: integer;
begin
  inherited;

  mat := TMaterialInfo.Create();
  mat.mName := 'A';
  mat.mDisplayName := 'Material A';
  mat.mDeliveryTime := 4;
  mat.mMinDeliverySize := 7;
  mat.mDeliveryInc := 2;
  mat.mReserveStock := 0;
  mWorkspace.addMaterial(mat);

  // Add work, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat;
  reqmat.mAmout := 8; // !!!!!!!! amount > 7, but < 7+2 (2 is increment)
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work := TManufactureWorkItem.Create('work', 42, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work);

  mMrp.formOrders();

  // Result of the test should be one material order
  // of material A with ammount 9 (min delivery size of A + one increment)
  // with finish time 42 (work start time)

  order:= nil;
  for item in mWorkspace.getItems() do begin
     if item.getType() = MaterialOrder then begin
       order := item as TMaterialOrderItem;
     end;
  end;
  Check(order <> nil, 'There is no material order');
  Check(order.getMaterialAmount() = (mat.mMinDeliverySize + mat.mDeliveryInc), 'Invalid amount');

end;

procedure TMrpTests.TwoParallelWorksOnOrder;
var order : TMaterialOrderItem;
 mat: TMaterialInfo;
 work: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
 item: TWorkspaceItem;
 count: integer;
begin
  inherited;

  mat := TMaterialInfo.Create();
  mat.mName := 'A';
  mat.mDisplayName := 'Material A';
  mat.mDeliveryTime := 4;
  mat.mMinDeliverySize := 7;
  mat.mDeliveryInc := 2;
  mat.mReserveStock := 0;
  mWorkspace.addMaterial(mat);

  // Add work 1, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat;
  reqmat.mAmout := 5;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work := TManufactureWorkItem.Create('work', 42, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work);

  // Add work 2, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat;
  reqmat.mAmout := 2;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work := TManufactureWorkItem.Create('work', 42, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work);

  mMrp.formOrders();

  // Result of the test should be one material order
  // of material A with ammount 7 (min delivery size of A and 5+2)
  // with finish time 42 (works start time)

  Check(mWorkspace.getItems().Count = 3, 'Invalid item count (should be 3 - 2 work items + one order)');

  order:= nil;
  count := 0;
  for item in mWorkspace.getItems() do begin
     if item.getType() = MaterialOrder then begin
       order := item as TMaterialOrderItem;
       inc(count);
     end;
  end;
  Check(order <> nil, 'There is no material order');
  Check(count = 1, 'There is more then one order');
  Check(order.getMaterialName() = 'A', 'Invalid material');
  Check(order.getMaterialAmount() = mWorkspace.getMaterialInfo('A').mMinDeliverySize, 'Invalid amount');
  Check(order.getFinishTime() = work.getStartTime()-1, 'Invalid finish time');
  Check(order.getStartTime() = (work.getStartTime() - mat.mDeliveryTime), 'Invalid start time');

end;


procedure TMrpTests.TwoNotParallelWorksTwoOrders;
var order1, order2, swap : TMaterialOrderItem;
 mat: TMaterialInfo;
 work1, work2: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
 item: TWorkspaceItem;
 count: integer;
begin
  inherited;

  mat := TMaterialInfo.Create();
  mat.mName := 'A';
  mat.mDisplayName := 'Material A';
  mat.mDeliveryTime := 4;
  mat.mMinDeliverySize := 7;
  mat.mDeliveryInc := 2;
  mat.mReserveStock := 0;
  mWorkspace.addMaterial(mat);

  // Add work 1, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat;
  reqmat.mAmout := 5;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work1 := TManufactureWorkItem.Create('work', 42, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work1);

  // Add work 2, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat;
  reqmat.mAmout := 9;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work2 := TManufactureWorkItem.Create('work', 50, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work2);

  mMrp.formOrders();

  // Result of the test should be two material orders
  // of material A with ammount 7 (first - min delivery size) and 7 (7-5-9 = -7)
  // with finish time 42 (work 1 start time) and 50 (work 2 start time)

  Check(mWorkspace.getItems().Count = 4, 'Invalid item count (should be 4 - 2 work items + 2 orders)');

  order1:= nil;
  for item in mWorkspace.getItems() do begin
     if item.getType() = MaterialOrder then begin
        if order1 = nil then
          order1 := item as TMaterialOrderItem
        else
          order2 := item as TMaterialOrderItem;
     end;
  end;
  Check((order1 <> nil) and (order2 <> nil), 'There is no two material orders');

  // Make order1 for work1, order2 for work2
  if order2.getFinishTime() = work1.getStartTime() then begin
    swap := order1;
    order1 := order2;
    order2 := swap;
  end;
  Check(order1.getFinishTime() = work1.getStartTime()-1, 'Invalid finish time 1');
  Check(order2.getFinishTime() = work2.getStartTime()-1, 'Invalid finish time 2');

  Check(order1.getMaterialAmount() = 7, 'Invalid amount of first order');
  Check(order2.getMaterialAmount() = 7, 'Invalid amount of second order');

end;

procedure TMrpTests.TwoWorksTwoMaterials;
var order1, order2, swap : TMaterialOrderItem;
 mat1, mat2: TMaterialInfo;
 work1, work2: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
 item: TWorkspaceItem;
 count: integer;
begin
  inherited;

  mat1 := TMaterialInfo.Create();
  mat1.mName := 'A';
  mat1.mDisplayName := 'Material A';
  mat1.mDeliveryTime := 4;
  mat1.mMinDeliverySize := 7;
  mat1.mDeliveryInc := 2;
  mat1.mReserveStock := 0;
  mWorkspace.addMaterial(mat1);

  mat2 := TMaterialInfo.Create();
  mat2.mName := 'B';
  mat2.mDisplayName := 'Material B';
  mat2.mDeliveryTime := 5;
  mat2.mMinDeliverySize := 2;
  mat2.mDeliveryInc := 1;
  mat2.mReserveStock := 0;
  mWorkspace.addMaterial(mat2);

  // Add work 1, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat1;
  reqmat.mAmout := 5;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work1 := TManufactureWorkItem.Create('work', 42, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work1);

  // Add work 2, that requres B
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat2;
  reqmat.mAmout := 9;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work2 := TManufactureWorkItem.Create('work', 50, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work2);

  mMrp.formOrders();
  ShowMessage(mMRP.generateReport);

  // Result of the test should be two material orders:
  // one for material A with ammount 7 (min delivery size)
  // with finish time 42 (work 1 start time)
  // and one for material B with amount 9 (requirement) and finish time 50 (work2 start time)

  Check(mWorkspace.getItems().Count = 4, 'Invalid item count (should be 4: 2 work items + 2 orders)');

  order1:= nil;
  for item in mWorkspace.getItems() do begin
     if item.getType() = MaterialOrder then begin
        if order1 = nil then
          order1 := item as TMaterialOrderItem
        else
          order2 := item as TMaterialOrderItem;
     end;
  end;
  Check((order1 <> nil) and (order2 <> nil), 'There is no two material orders');

  // Make order1 for work1, order2 for work2
  if order2.getFinishTime() = work1.getStartTime() then begin
    swap := order1;
    order1 := order2;
    order2 := swap;
  end;
  Check(order1.getFinishTime() = work1.getStartTime()-1, 'Invalid finish time 1');
  Check(order2.getFinishTime() = work2.getStartTime()-1, 'Invalid finish time 2');

  Check(order1.getMaterialAmount() = 7, 'Invalid amount of first order');
  Check(order2.getMaterialAmount() = 9, 'Invalid amount of second order');

end;

initialization

 TestFramework.RegisterTest(TMrpTests.Suite);

end.
