unit ReportWindow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TReportView = class(TForm)
    Button1: TButton;
    Edit1: TMemo;
    procedure Button1Click(Sender: TObject);
  public
     procedure setReportText(text: string);
  end;

var
  ReportView: TReportView;

implementation

{$R *.dfm}

{ TReportView }

procedure TReportView.Button1Click(Sender: TObject);
begin
  Hide();
end;

procedure TReportView.setReportText(text: string);
begin
  Edit1.Text := text;
end;

end.
