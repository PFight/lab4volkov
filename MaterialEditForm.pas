unit MaterialEditForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Workspace;

type
  TMaterialEditWnd = class(TForm)
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    EditName: TEdit;
    SpinEditDeliveryTime: TSpinEdit;
    SpinEditMinDeliverySize: TSpinEdit;
    SpinEditDeliverySizeInc: TSpinEdit;
    SpinEditReserveStock: TSpinEdit;
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    mMaterial: TMaterialInfo;


  public
    procedure setMaterial(mat: TMaterialInfo);
    function getMaterial(): TMaterialInfo;
  end;


implementation

{$R *.dfm}

{ TMaterialEditWnd }

procedure TMaterialEditWnd.ButtonCancelClick(Sender: TObject);
begin
  ModalResult:= mrCancel;
  Close();
end;

procedure TMaterialEditWnd.ButtonOkClick(Sender: TObject);
begin
  mMaterial.mDeliveryInc := SpinEditDeliverySizeInc.Value;
  mMaterial.mDeliveryTime := SpinEditDeliveryTime.Value;
  mMaterial.mMinDeliverySize := SpinEditMinDeliverySize.Value;
  mMaterial.mReserveStock := SpinEditReserveStock.Value;
  mMaterial.mDisplayName := EditName.Text;
  ModalResult:= mrOk;
  CloseModal();
end;

function TMaterialEditWnd.getMaterial: TMaterialInfo;
begin
  getMaterial:= mMaterial;
end;

procedure TMaterialEditWnd.setMaterial(mat: TMaterialInfo);
begin
  mMaterial := mat;
  EditName.Text := mMaterial.mDisplayName;
  SpinEditDeliveryTime.Value := mMaterial.mDeliveryTime;
  SpinEditMinDeliverySize.Value := round(mMaterial.mMinDeliverySize);
  SpinEditDeliverySizeInc.Value := round(mMaterial.mDeliveryInc);
  SpinEditReserveStock.Value := round(mMaterial.mReserveStock);
end;

end.
