unit MRP;

interface

uses Workspace, Generics.Collections, SysUtils;

type

  TStock = record
    mName: string;
    mAmount: double;
    mReserve: double;
  end;

  TMrp = class
  public
    constructor Create(workspace: TWorkspace);

    /// Analyse workspace items (manufacturing works and existing orders)
    ///  and create new items-orders, to fulfill all material requirements.
    procedure formOrders();

    /// Form report, that contains final result of MRP alhoritm: list
    ///  of what and when we should to order. This report will be used
    ///  as output of the program.
    function generateReport(): string;

  private
    mWorkspace: TWorkspace;
    mStock: array of TStock;
    procedure ReInitStock;
    procedure DoOrder(Time: integer);
    function CalcAmount(MinSize, Increment, AmountOnStock, ReserveStock: double): double;
  end;

//function SortL(p1, p2: Pointer): integer;

implementation

{ TMrp }

{function SortL(p1, p2: pointer): integer;
var Item1, Item2: TManufactureWorkItem;
begin
  Item1 := TManufactureWorkItem(p1);
  Item2 := TManufactureWorkItem(p2);

  if Item1.getStartTime > Item2.getStartTime then Result := 1
  else if Item1.getStartTime = Item2.getStartTime then Result := 0
  else Result := -1;
end;}

procedure TMrp.ReInitStock;
var MatInfo: TList<TMaterialInfo>;
    i: Integer;
    MatCount: integer;
begin
  MatInfo := mWorkspace.getMaterials;
  MatCount := MatInfo.Count;
  SetLength(mStock, 0);
  SetLength(mStock, MatCount);
  for i := 0 to MatCount - 1 do
  begin
    mStock[i].mName := MatInfo.Items[i].mName;
    mStock[i].mAmount := 0;
    mStock[i].mReserve := MatInfo.Items[i].mReserveStock;
  end;
end;

constructor TMrp.Create(workspace: TWorkspace);
begin
  self.mWorkspace := workspace;
end;

procedure TMrp.formOrders;
var Works: TList<TManufactureWorkItem>;
    Item: TWorkSpaceItem;
    RecMat: TMaterialRequirement;
    i: integer;
    CurTime: integer;
  j: Integer;
    tmp: TManufactureWorkItem;
begin
  ReInitStock;
  Works := TList<TManufactureWorkItem>.Create;
  for Item in mWorkSpace.getItems do
    if Item is TManufactureWorkItem then
      Works.Add(Item as TManufactureWorkItem);
  //Works.Sort(SortL);

  for i := 0 to Works.Count - 2 do
    for j := 0 to Works.Count - i - 2 do
      if Works.Items[j].getStartTime > Works.Items[j + 1].getStartTime then
      begin
        tmp := Works.Items[j];
        Works.Items[j] := Works.Items[j + 1];
        Works.Items[j + 1] := tmp;
      end;

  i := 0;
  while i <= Works.Count - 1 do
  begin
    CurTime := Works.Items[i].getStartTime;
    while (i <= Works.Count - 1) and (Works.Items[i].getStartTime = CurTime) do
    begin
      for RecMat in Works.Items[i].getRequiredMaterials do
        for j := 0 to Length(mStock) - 1 do
          if mStock[j].mName = RecMat.mMaterial.mName then
            mStock[j].mAmount := mStock[j].mAmount - RecMat.mAmout;
      Inc(i);
    end;
    DoOrder(CurTime);
  end;
end;

procedure TMRP.DoOrder(Time: integer);
var i: integer;
    MaterialOrderItem: TMaterialOrderItem;
    Material: TMaterialInfo;
    Amount: double;
begin
  for i := 0 to Length(mStock) - 1 do
    if (mStock[i].mAmount - mStock[i].mReserve) < 0 then
    begin
      Material := mWorkSpace.getMaterialInfo(mStock[i].mName);
      Amount := CalcAmount(Material.mMinDeliverySize, Material.mDeliveryInc,
        mStock[i].mAmount, mStock[i].mReserve);
      MaterialOrderItem := TMaterialOrderItem.Create(mStock[i].mName,
        Amount, Time - Material.mDeliveryTime, self.mWorkspace);
      mWorkSpace.addItem(MaterialOrderItem);
      mStock[i].mAmount := mStock[i].mAmount + Amount;
    end;
end;

function TMRP.CalcAmount(MinSize: Double; Increment: Double;
  AmountOnStock: Double; ReserveStock: double): double;
var AAmount, ADeliverySize: double;
begin
  AAmount := AmountOnStock - ReserveStock;
  AAmount := AAmount + MinSize;
  ADeliverySize := MinSize;
  while AAmount < 0 do
  begin
    AAmount := AAmount + Increment;
    ADeliverySize := ADeliverySize + Increment;
  end;
  Result := ADeliverySize;
end;

function TMrp.generateReport: string;
var Item: TWorkSpaceItem;
    Report: string;
begin
  Report := '';
  for Item in mWorkSpace.getItems do
    if Item is TMaterialOrderItem then
    begin
      Report := Report + '' + FormatDateTime('dd/mm/yyyy', Item.getStartTime);
      With Item as TMaterialOrderItem do
        Report := Report + ': order ' + FloatToStrF(getMaterialAmount, ffGeneral, 4, 2) +
         ' units of material "' + getMaterialName + '" '  + #13#10;
    end;
  Result := Report;
end;

end.
