unit EditMaterialReqForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Workspace, MaterialsForm;

type
  TEditMaterialReqWnd = class(TForm)
    ComboBoxMaterial: TComboBox;
    SpinEditAmount: TSpinEdit;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    ButtonEditMaterials: TButton;
    ButtonOK: TButton;
    ButtonCancel: TButton;
    procedure ButtonOKClick(Sender: TObject);
    procedure ButtonEditMaterialsClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    mMatReq:TMaterialRequirement;
    mWorkspace: TWorkspace;
    mMaterialsDlg: TMaterialsWnd;
  public
    constructor Create(workspace: TWorkspace; parent: TControl);
    procedure setMaterialReq(obj:TMaterialRequirement);
  end;

var
  EditMaterialReqWnd: TEditMaterialReqWnd;

implementation

{$R *.dfm}

{ TEditMaterialReqWnd }

procedure TEditMaterialReqWnd.ButtonCancelClick(Sender: TObject);
begin
  Close();
end;

procedure TEditMaterialReqWnd.ButtonEditMaterialsClick(Sender: TObject);
var mat: TMaterialInfo;
begin
  mMaterialsDlg.ShowModal();
  ComboBoxMaterial.Items.Clear();
  for mat in mWorkspace.getMaterials do begin
    ComboBoxMaterial.AddItem(mat.mDisplayName, mat);
  end;
end;

procedure TEditMaterialReqWnd.ButtonOKClick(Sender: TObject);
begin
  if ComboBoxMaterial.ItemIndex >=0 then begin
    mMatReq.mAmout := SpinEditAmount.Value;
    mMatreq.mMaterial := ComboBoxMaterial.Items.Objects[ComboBoxMaterial.ItemIndex] as TMaterialInfo;
    ModalResult:= mrOk;
    CloseModal();
  end
  else MessageDlg('Select material, please.', mtInformation, [mbOK], 0);

end;

constructor TEditMaterialReqWnd.Create(workspace: TWorkspace; parent: TControl);
var mat: TMaterialInfo;
begin
  inherited Create(parent);
  mWorkspace := workspace;
  for mat in workspace.getMaterials do begin
    ComboBoxMaterial.AddItem(mat.mDisplayName, mat);
  end;
  mMaterialsDlg:= TMaterialsWnd.Create(self);
  mMaterialsDlg.setMaterials(mWorkspace.getMaterials);
end;

procedure TEditMaterialReqWnd.setMaterialReq(obj: TMaterialRequirement);
var i:integer;
begin
  mMatReq:= obj;
  SpinEditAmount.Value := round(mMatReq.mAmout);
  // Select obj.mMaterial
  for i := 0 to ComboBoxMaterial.Items.Count-1 do begin
    if ComboBoxMaterial.Items.Objects[i] = obj.mMaterial then begin
      ComboBoxMaterial.ItemIndex:= i;
      break;
    end;
  end;
end;

end.
