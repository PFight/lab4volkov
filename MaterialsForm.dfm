object MaterialsWnd: TMaterialsWnd
  Left = 0
  Top = 0
  Caption = 'MaterialsWnd'
  ClientHeight = 303
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    347
    303)
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 231
    Height = 282
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object ButtonAdd: TButton
    Left = 245
    Top = 8
    Width = 94
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Add'
    TabOrder = 1
    OnClick = ButtonAddClick
  end
  object ButtonRemove: TButton
    Left = 245
    Top = 70
    Width = 94
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Remove'
    TabOrder = 2
    OnClick = ButtonRemoveClick
  end
  object ButtonEdit: TButton
    Left = 245
    Top = 39
    Width = 94
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Edit'
    TabOrder = 3
    OnClick = ButtonEditClick
  end
  object ButtonOk: TButton
    Left = 264
    Top = 265
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1054#1050
    Default = True
    TabOrder = 4
    OnClick = ButtonOkClick
  end
end
