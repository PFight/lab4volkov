unit WTest;

interface

uses

 TestFrameWork, Workspace, Generics.Collections;

type

 TWorkspaceTests = class(TTestCase)

 protected
    mWorkspace: TWorkspace;

    procedure SetUp; override;
    procedure TearDown; override;
 published
    procedure TestMaterials;
    procedure TestWorkItems;
    procedure TestOrderItems;
    procedure TestOrderDeliveryTime;
 end;

implementation


procedure TWorkspaceTests.SetUp;
begin
  inherited;
  mWorkspace := TWorkspace.Create();
end;

procedure TWorkspaceTests.TearDown;
begin
  inherited;
  mWorkspace.Free();
end;

procedure TWorkspaceTests.TestMaterials;
var mat: TMaterialInfo;
begin
  mat := TMaterialInfo.Create();
  mat.mName := 'name';
  mat.mDisplayName := 'dispalayName';
  mWorkspace.addMaterial(mat);

  Check(mat = mWorkspace.getMaterials().First, 'Item doesnt added');

  mWorkspace.removeMaterial(mat);

  Check(mWorkspace.getMaterials().Count = 0, 'Item doesnt removed');
end;

procedure TWorkspaceTests.TestOrderDeliveryTime;
  var order: TMaterialOrderItem;
  mat: TMaterialInfo;
begin
  mat := TMaterialInfo.Create();
  mat.mName := 'm1';
  mat.mDeliveryTime := 2;
  mWorkspace.addMaterial(mat);

  order := TMaterialOrderItem.Create('m1', 3, 0, mWorkspace);
  mWorkspace.addItem(order);

  Check(order.getFinishTime = 0 + mat.mDeliveryTime-1, 'Invalid delivery time calculation');

  mWorkspace.removeItem(order);
  mWorkspace.removeMaterial(mat);
end;

procedure TWorkspaceTests.TestOrderItems;
var order: TMaterialOrderItem;
  mat: TMaterialInfo;
begin
  mat := TMaterialInfo.Create();
  mat.mName := 'm1';
  mat.mDeliveryTime := 2;
  mWorkspace.addMaterial(mat);

  order := TMaterialOrderItem.Create('m1', 3, 0, mWorkspace);
  mWorkspace.addItem(order);

  Check(order.getType() = MaterialOrder, 'Invalid type');
  Check(order = mWorkspace.getItems().First, 'Item doesnt added');

  mWorkspace.removeItem(order);

  Check(mWorkspace.getItems().Count = 0, 'Item doesnt removed');

  mWorkspace.removeMaterial(mat);
end;

procedure TWorkspaceTests.TestWorkItems;
var
  work: TManufactureWorkItem;
  reqMatList: TList<TMaterialRequirement>;
  reqMat: TMaterialRequirement;
begin
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := nil;
  reqmat.mAmout := 3;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);

  work := TManufactureWorkItem.Create('work', 0, 10, reqMatList, mWorkspace);
  mWorkspace.addItem(work);

  Check(work.getType() = ManufactureWork, 'Invalid type');
  Check(work = mWorkspace.getItems().First, 'Item doesnt added');

  mWorkspace.removeItem(work);

  Check(mWorkspace.getItems().Count = 0, 'Item doesnt removed');

end;

initialization

 TestFramework.RegisterTest(TWorkspaceTests.Suite);

end.
