unit WorkEditForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Workspace, EditMaterialReqForm;

type
  TWorkEditWnd = class(TForm)
    DateTimePicker2: TDateTimePicker;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    ListBoxMaterials: TListBox;
    StaticText3: TStaticText;
    ButtonAddMeterial: TButton;
    ButtonRemoveMaterial: TButton;
    EditName: TEdit;
    StaticText4: TStaticText;
    OK: TButton;
    Cancel: TButton;
    DateTimePicker1: TDateTimePicker;
    procedure OKClick(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure ButtonAddMeterialClick(Sender: TObject);
    procedure ButtonRemoveMaterialClick(Sender: TObject);
  private
    mWork :TManufactureWorkItem;
    mEditMaterialReq: TEditMaterialReqWnd; 

    procedure updateMaterials();
  public
    constructor Create(workspace: TWorkspace; parent: TControl);
  
    procedure setWork(work:TManufactureWorkItem);
  end;

var
  WorkEditWnd: TWorkEditWnd;



implementation

{$R *.dfm}

{ TWorkEditWnd }

procedure TWorkEditWnd.ButtonAddMeterialClick(Sender: TObject);
var matReq: TMaterialRequirement;
begin
  matReq := TMaterialRequirement.Create;
  mEditMaterialReq.setMaterialReq(matReq);
  if mEditMaterialReq.ShowModal() = mrOk then begin
    ListBoxMaterials.AddItem(matReq.mMaterial.mDisplayName + ' (amount: ' + IntToStr(round(matReq.mAmout)) + ')', matReq);
  end
  else matReq.Free;  
end;

procedure TWorkEditWnd.ButtonRemoveMaterialClick(Sender: TObject);
begin
  if ListBoxMaterials.ItemIndex >=0 then begin
     if MessageDlg('Really?',
            mtConfirmation, [mbYes, mbNo], 0) = mrYes  then begin
       ListBoxMaterials.Items.Delete(ListBoxMaterials.ItemIndex);
    end;
  end
  else MessageDlg('Select material, please.', mtInformation, [mbOK], 0);
end;

procedure TWorkEditWnd.CancelClick(Sender: TObject);
begin
   ModalResult := mrCancel;
  Close();
end;

constructor TWorkEditWnd.Create(workspace: TWorkspace; parent: TControl);
begin
  mEditMaterialReq:= TEditMaterialReqWnd.Create(workspace, self);
  inherited Create(parent);
end;

procedure TWorkEditWnd.OKClick(Sender: TObject);
var i:integer;
begin
  mWork.setName(EditName.Text);
  mWork.setStartTime(Trunc(DateTimePicker1.Date));
  mWork.setFinishTime(Trunc(DateTimePicker2.Date));
  DateTimePicker1.Date := mWork.getStartTime;
  DateTimePicker2.Date := mWork.getFinishTime;
  mWork.setStartTime(Trunc(DateTimePicker1.Date));
  mWork.setFinishTime(Trunc(DateTimePicker2.Date));

  mWork.getRequiredMaterials().Clear;
  for i:=0 to ListBoxMaterials.Items.Count -1 do begin
       mWork.getRequiredMaterials.Add(ListBoxMaterials.Items.Objects[i] as TMaterialRequirement);
   end;
   ModalResult := mrOk;
   closeModal();
end;

procedure TWorkEditWnd.setWork(work: TManufactureWorkItem);
begin
  mWork := work;
  EditName.Text := work.getName();
  DateTimePicker1.Date := work.getStartTime;
  DateTimePicker2.Date := work.getFinishTime;
  updateMaterials();
end;

procedure TWorkEditWnd.updateMaterials;
var mat: TMaterialRequirement;
begin
   ListBoxMaterials.Clear;
   for mat in mWork.getRequiredMaterials() do begin
       ListBoxMaterials.AddItem(mat.mMaterial.mDisplayName + ' (amount: ' + IntToStr(round(mat.mAmout)) + ')', mat);
   end;     
end;

end.
