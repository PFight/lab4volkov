unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Workspace, StdCtrls, MaterialsForm, Spin, ComCtrls, ToolWin, ExtCtrls, Generics.Collections,
  WorkEditForm;

type
  TEvent = procedure() of object;

  TMainWnd = class(TForm)
    ButtonRunMrp: TButton;
    Panel1: TPanel;
    ButtonLater: TButton;
    ButtonEralier: TButton;
    ButtonAddWork: TButton;
    ButtonRemove: TButton;
    ButtonEdit: TButton;
    Button1: TButton;
    Button2: TButton;
    procedure ButtonRunMrpClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ButtonLaterClick(Sender: TObject);
    procedure ButtonEralierClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonAddWorkClick(Sender: TObject);
    procedure ButtonEditClick(Sender: TObject);
    procedure ButtonRemoveClick(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  private
    mOnRunMrpCommand: TEvent;
    mWorkspace: TWorkspace;
    mMaterialsDlg: TMaterialsWnd;
    mMinTime, mMaxTime: integer;
    mMarks: TList<TStaticText>;
    mItems: TList<TStaticText>;
    mPixelsPerTimeUnit: integer;
    mSelectedLabel: TStaticText;

    procedure configurePanel(minTime, maxTime: integer);
    procedure OnLabelClick(Sender: TObject);
  public

    property Workspace: TWorkspace read mWorkspace write mWorkspace;

    property OnRunMrpCommand: TEvent read mOnRunMrpCommand write mOnRunMrpCommand;

    procedure syncWithWorkspace();
  end;

var
  MainWnd: TMainWnd;
  resourcestring greetings = 'Hello world';

implementation

{$R *.dfm}

{ TForm1 }

procedure TMainWnd.ButtonRemoveClick(Sender: TObject);
var work, toDelete: TWorkspaceItem;
begin
   if mSelectedLabel <> nil then begin
    if MessageDlg('Really?', mtConfirmation, [mbYes, mbNo], 0) = mrYes  then begin
      toDelete := nil;
      for work in mWorkspace.getItems do begin
        if work.getID = mSelectedLabel.Tag then begin
            toDelete:= work;
            break;
        end;
      end;
      mWorkspace.removeItem(toDelete);
      Panel1.RemoveControl(mSelectedLabel);
      mSelectedLabel:=nil;
      syncWithWorkspace();
    end;
  end
  else MessageDlg('Select item, please.', mtInformation, [mbOK], 0);
end;

procedure TMainWnd.ButtonRunMrpClick(Sender: TObject);
begin
  if Assigned(mOnRunMrpCommand) then mOnRunMrpCommand();
  Caption := greetings;
end;

procedure TMainWnd.Button2Click(Sender: TObject);
begin
  if mMaterialsDlg = nil then
    mMaterialsDlg:= TMaterialsWnd.Create(self);

  mMaterialsDlg.setMaterials(mWorkspace.getMaterials());
  mMaterialsDlg.Show();
end;

procedure TMainWnd.ButtonLaterClick(Sender: TObject);
begin
  Panel1.Left := Panel1.Left - mPixelsPerTimeUnit;
  if Panel1.Left + Panel1.Width < Width + mPixelsPerTimeUnit then begin
    configurePanel(mMinTime, mMaxTime+10);
  end;
end;

procedure TMainWnd.ButtonAddWorkClick(Sender: TObject);
var work: TManufactureWorkItem;
  dlg: TWorkEditWnd;
begin
  work := TManufactureWorkItem.Create('Work', Trunc(Date), Trunc(Date)+1, TList<TMaterialRequirement>.Create(), mWorkspace);
  dlg:= TWorkEditWnd.Create(mWorkspace, self);
  dlg.setWork(work);
  if dlg.ShowModal = mrOk then begin
    mWorkspace.addItem(work);
    syncWithWorkspace();
  end
  else begin
    work.Free;
  end;
  
end;

procedure TMainWnd.ButtonEditClick(Sender: TObject);
var dlg: TWorkEditWnd;
  work: TWorkspaceItem;
begin
  if mSelectedLabel <> nil then begin
    dlg:= TWorkEditWnd.Create(mWorkspace, self);
    for work in mWorkspace.getItems do begin
      if work.getID = mSelectedLabel.Tag then begin
          dlg.setWork(work as TManufactureWorkItem);
          break;
      end;
    end;
    dlg.showModal();
    syncWithWorkspace();
  end
  else MessageDlg('Select item, please.', mtInformation, [mbOK], 0);
end;

procedure TMainWnd.ButtonEralierClick(Sender: TObject);
begin
  Panel1.Left := Panel1.Left + mPixelsPerTimeUnit;
  if Panel1.Left > -mPixelsPerTimeUnit then begin
    configurePanel(mMinTime - 10, mMaxTime);
    Panel1.Left := Panel1.Left - 10*mPixelsPerTimeUnit;
  end;
end;

procedure TMainWnd.configurePanel(minTime, maxTime: integer);
var mark: TStaticText;
 time: integer;
begin
  for mark in mMarks do begin
    Panel1.RemoveControl(mark);
  end;
  Panel1.Width := (maxTime - minTime) * mPixelsPerTimeUnit + 2*mPixelsPerTimeUnit;
  for time := 0 to maxTime-minTime do begin
    mark:= TStaticText.Create(Panel1);
    ShortDateFormat := 'dd/mm/yyyy';
    mark.Caption := '| ' + DateToStr(time+minTime);
    mark.Parent := Panel1;
    mark.Left := time * mPixelsPerTimeUnit;
    mark.Top := 5;
    mark.Width := mPixelsPerTimeUnit;
    mark.Height := 17;
    mark.Show;
    mMarks.Add(mark);
  end; 
  mMinTime := minTime;
  mMaxTime := maxTime;
  
  syncWithWorkspace();
end;

procedure TMainWnd.FormCreate(Sender: TObject);
var order1, order2, swap : TMaterialOrderItem;
 mat1, mat2: TMaterialInfo;
 work1, work2: TManufactureWorkItem;
 reqMatList: TList<TMaterialRequirement>;
 reqMat: TMaterialRequirement;
begin
  mWorkspace := TWorkspace.Create;

  mat1 := TMaterialInfo.Create();
  mat1.mName := 'A';
  mat1.mDisplayName := 'Material A';
  mat1.mDeliveryTime := 4;
  mat1.mMinDeliverySize := 7;
  mat1.mDeliveryInc := 2;
  mat1.mReserveStock := 0;
  mWorkspace.addMaterial(mat1);

  mat2 := TMaterialInfo.Create();
  mat2.mName := 'B';
  mat2.mDisplayName := 'Material B';
  mat2.mDeliveryTime := 5;
  mat2.mMinDeliverySize := 2;
  mat2.mDeliveryInc := 1;
  mat2.mReserveStock := 0;
  mWorkspace.addMaterial(mat2);

  // Add work 1, that requres A
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat1;
  reqmat.mAmout := 5;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work1 := TManufactureWorkItem.Create('Another work', Trunc(Date), Trunc(Date) + 1, reqMatList, mWorkspace);
  mWorkspace.addItem(work1);

  // Add work 2, that requres B
  reqMat:= TMaterialRequirement.Create();
  reqmat.mMaterial := mat2;
  reqmat.mAmout := 9;
  reqMatList := TList<TMaterialRequirement>.Create();
  reqMatList.Add(reqMat);
  work2 := TManufactureWorkItem.Create('Some work', Trunc(Date)-2, Trunc(Date), reqMatList, mWorkspace);
  mWorkspace.addItem(work2);

  mSelectedLabel:= nil;

  mPixelsPerTimeUnit:= 100;
  mMarks :=  TList<TStaticText>.Create();
  mItems :=  TList<TStaticText>.Create();
  configurePanel(Trunc(Date) - 10, Trunc(Date)+ 10);
  mPixelsPerTimeUnit:= 100;
end;

procedure TMainWnd.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
   if WheelDelta > 0 then ButtonEralierClick(ButtonLater)
   else  ButtonLaterClick(ButtonLater);
end;

procedure TMainWnd.OnLabelClick(Sender: TObject);
var workspaceItem, wi: TWorkspaceItem;
 lbl: TStaticText;
begin

  wi := nil;
  for workspaceItem in mWorkspace.getItems() do begin
    if (Sender as TStaticText).Tag = workspaceItem.getID then begin
      wi := workspaceItem;
      break;
    end;
  end;
  if wi.getType = ManufactureWork then begin
    if mSelectedLabel <> nil then begin
      mSelectedLabel.Color := clMoneyGreen;
    end;
    mSelectedLabel:= Sender as TStaticText;
    mSelectedLabel.Color := clGreen;
  end;
end;

procedure TMainWnd.syncWithWorkspace;
var workspaceItem: TWorkspaceItem;
  lbl, lbl1, lbl2, itemLbl: TStaticText;
  right1, right2: integer;
  found : boolean;
  i: integer;
begin
  // Remove removed
  i:=0;
  while i < mItems.Count do begin
   found := false;
   for workspaceItem in mWorkspace.getItems() do begin
      if mItems[i].Tag = workspaceItem.getID then begin
        found:= true;
      end;
   end;
   if not found then begin
      Panel1.RemoveControl(mItems[i]);
      mItems[i].Free();
      mItems.Remove(mItems[i]);
   end
   else inc(i);
  end;

  // Add or update for every workspace item label to panel
  for workspaceItem in mWorkspace.getItems() do begin
    itemLbl:= nil;
    for lbl in mItems do begin
      if lbl.Tag = workspaceItem.getID then begin
        itemLbl := lbl;
      end;
    end;
    if itemLbl = nil then begin
      itemLbl:= TStaticText.Create(Panel1);
      mItems.Add(itemLbl);
    end;
    itemLbl.Caption := workspaceItem.getName();
    itemLbl.Parent := Panel1;
    if workspaceItem.getType = ManufactureWork then
      itemLbl.Color := clMoneyGreen
    else
      itemLbl.Color := clLtGray;
    if itemLbl = mSelectedLabel then OnLabelClick(itemLbl);    
    itemLbl.Left := (workspaceItem.getStartTime - mMinTime) * mPixelsPerTimeUnit;
    itemLbl.Top := 23;
    itemLbl.Tag := workspaceItem.getID();
    itemLbl.OnClick := OnLabelClick;
    itemLbl.Width := (workspaceItem.getFinishTime - workspaceItem.getStartTime +1) * mPixelsPerTimeUnit;
    if workspaceItem.getFinishTime = workspaceItem.getStartTime then  begin
      itemLbl.Width :=  mPixelsPerTimeUnit;
    end;
    itemLbl.Height := 17;
    itemLbl.Show;
  end;
  // Stack vertically overlaping labels
  for lbl1 in mItems do begin
    for lbl2 in mItems do begin
      if lbl1 <> lbl2 then begin
        // if overlaping
        right1:= lbl1.Left + lbl1.Width;
        right2:= lbl2.Left + lbl2.Width;
        if (((lbl2.Left <= lbl1.Left) and (lbl1.Left <= right2)) or
            ((lbl2.Left <= right1) and (right1 <= right2))) then begin
          if (lbl1.Top = lbl2.Top) then begin
            lbl2.Top := lbl2.Top + 18; 
          end;
        end; 
      end;
    end;
  end;
  
end;

end.
