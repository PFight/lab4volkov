object MaterialEditWnd: TMaterialEditWnd
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Material properties'
  ClientHeight = 167
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = 11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 11
  object StaticText1: TStaticText
    Left = 77
    Top = 19
    Width = 32
    Height = 15
    Caption = 'Name:'
    TabOrder = 0
  end
  object StaticText2: TStaticText
    Left = 34
    Top = 63
    Width = 77
    Height = 15
    Caption = 'Min delivery size:'
    TabOrder = 1
  end
  object StaticText3: TStaticText
    Left = 47
    Top = 41
    Width = 64
    Height = 15
    Caption = 'Delivery time:'
    TabOrder = 2
  end
  object StaticText4: TStaticText
    Left = 7
    Top = 84
    Width = 104
    Height = 15
    Caption = 'Delivery size increment:'
    TabOrder = 3
  end
  object EditName: TEdit
    Left = 112
    Top = 16
    Width = 156
    Height = 19
    TabOrder = 4
  end
  object SpinEditDeliveryTime: TSpinEdit
    Left = 112
    Top = 36
    Width = 65
    Height = 19
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object SpinEditMinDeliverySize: TSpinEdit
    Left = 112
    Top = 56
    Width = 65
    Height = 19
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
  end
  object SpinEditDeliverySizeInc: TSpinEdit
    Left = 112
    Top = 80
    Width = 65
    Height = 19
    MaxValue = 0
    MinValue = 0
    TabOrder = 7
    Value = 0
  end
  object StaticText5: TStaticText
    Left = 42
    Top = 107
    Width = 65
    Height = 15
    Caption = 'Reserve stock:'
    TabOrder = 8
  end
  object SpinEditReserveStock: TSpinEdit
    Left = 112
    Top = 103
    Width = 65
    Height = 19
    MaxValue = 0
    MinValue = 0
    TabOrder = 9
    Value = 0
  end
  object ButtonOk: TButton
    Left = 111
    Top = 139
    Width = 82
    Height = 21
    Caption = #1054#1050
    Default = True
    TabOrder = 10
    OnClick = ButtonOkClick
  end
  object ButtonCancel: TButton
    Left = 198
    Top = 139
    Width = 82
    Height = 21
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 11
    OnClick = ButtonCancelClick
  end
end
