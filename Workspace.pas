unit Workspace;

interface

uses Generics.Collections, SysUtils;

// Forward declaration
type TWorkspace = class;

 TWorkspaceItemTypes = (MaterialOrder, ManufactureWork);

/// Base class for workspace items
 TWorkspaceItem = class
  public
    /// Needs workspace to generate id.
    constructor Create(name: string;
      startTime, finishTime: Integer;
      workspace: TWorkspace);

    /// Unique id, generated by TWorkspace
    function getID(): integer;

    /// Display name, that will be visible to user
    function getName(): string;
    procedure setName(name: string);

    /// For MaterialOrder - order point
    /// For ManufactureWork - start of the work
    function getStartTime(): Integer;
    procedure setStartTime(time: Integer);
    /// For MaterialOrder - time of the material arrive
    /// For ManufactureWork - time of work finish
    function getFinishTime(): Integer;
    procedure setFinishTime(time: Integer);

    function getType(): TWorkspaceItemTypes; virtual; abstract;

    function getWorkspace(): TWorkspace;

  private
    mID: integer;
    mName: string;
    mStartTime, mFinishTime: Integer;
    mWorkspace: TWorkspace;
end;

 TMaterialOrderItem = class(TWorkspaceItem)
  public

    constructor Create(materialName: string;
      amount: double;
      startTime: Integer;
      workspace: TWorkspace);

    function getMaterialName(): string;
    function getMaterialAmount(): double;
    procedure setMaterialAmount(amount: double);

    // Returns TWorkspaceItemTypes::MaterialOrder
    function getType(): TWorkspaceItemTypes; override;

  private
    mMaterialName: string;
    mMaterialAmmount: double;
end;

TMaterialInfo = class
  public
    mName: string;
    mDisplayName: string;
    mDeliveryTime: integer;
    mMinDeliverySize: double;
    mDeliveryInc: double;
    mReserveStock: double;
end;

 TMaterialRequirement = class
  public
    mMaterial: TMaterialInfo;
    mAmout: double;
end;

 TManufactureWorkItem = class(TWorkspaceItem)
  public
    constructor Create(name: string;
      startTime, finishTime: Integer;
      reqiredMaterials: TList<TMaterialRequirement>;
      workspace: TWorkspace);
    destructor Free;

    function getRequiredMaterials(): TList<TMaterialRequirement>;

    // Returns TWorkspaceItemTypes::ManufactureWork
    function getType(): TWorkspaceItemTypes; override;
  private
    mRequiredMaterials: TList<TMaterialRequirement>;
end;



 TWorkspace = class
  public

    constructor Create();
    destructor Free();

    function getItems(): TList<TWorkspaceItem>;
    procedure addItem(item: TWorkspaceItem);
    procedure removeItem(item: TWorkspaceItem);

    procedure clearOrders();

    procedure addMaterial(info: TMaterialInfo);
    procedure removeMaterial(info: TMaterialInfo);
    function getMaterials(): TList<TMaterialInfo>;
    function getMaterialInfo(materialName: string): TMaterialInfo;

    function generateItemID(): integer;
  private
    mItems: TList<TWorkspaceItem>;
    mMaterials: TList<TMaterialInfo>;
    mLastID: integer;

end;

implementation

{ TWorkspaceItem }

constructor TWorkspaceItem.Create(name: string; startTime,
  finishTime: Integer; workspace: TWorkspace);
begin
   mName := name;
   mStartTime:= startTime;
   mFinishTime:= finishTime;
   mWorkspace := workspace;
   mID := workspace.generateItemID();
end;

function TWorkspaceItem.getFinishTime: Integer;
begin
  getFinishTime:= mFinishTime;
end;

function TWorkspaceItem.getID: integer;
begin
   getID := mID;
end;

function TWorkspaceItem.getName: string;
begin
   getName := mName;
end;

function TWorkspaceItem.getStartTime: Integer;
begin
   getStartTime:= mStartTime;
end;

function TWorkspaceItem.getWorkspace: TWorkspace;
begin
   getWorkspace:= mWorkspace;
end;

procedure TWorkspaceItem.setFinishTime(time: Integer);
begin
   mFinishTime := time;
end;

procedure TWorkspaceItem.setName(name: string);
begin
   mName := name;
end;

procedure TWorkspaceItem.setStartTime(time: Integer);
begin
  mStartTime := time;
end;

{ TMaterialOrderItem }

constructor TMaterialOrderItem.Create(materialName: string; amount: double;
  startTime: Integer; workspace: TWorkspace);
var finishTime: Integer;
  name: string;
  material: TMaterialInfo;
begin
  material:= workspace.getMaterialInfo(materialName);
  assert(material <> nil, 'Material with name ' + materialName + ' doesnt exists');

  finishTime := startTime + material.mDeliveryTime -1;
  name := 'Order of ' + materialName + ' (' + FloatToStr(amount) + ')';
  mMaterialName:= materialName;
  mMaterialAmmount:= amount;
  inherited Create(name, startTime, finishTime, workspace);
end;

function TMaterialOrderItem.getMaterialAmount: double;
begin
   getMaterialAmount:= mMaterialAmmount;
end;

function TMaterialOrderItem.getMaterialName: string;
begin
   getMaterialName:= mMaterialName;
end;

function TMaterialOrderItem.getType: TWorkspaceItemTypes;
begin
   getType:= MaterialOrder;
end;

procedure TMaterialOrderItem.setMaterialAmount(amount: double);
begin
   mMaterialAmmount:= amount;
end;

{ TManufactureWorkItem }

constructor TManufactureWorkItem.Create(name: string; startTime,
  finishTime: Integer; reqiredMaterials: TList<TMaterialRequirement>;
  workspace: TWorkspace);
var item: TMaterialRequirement;
begin
  mRequiredMaterials:= TList<TMaterialRequirement>.Create();
  for item in reqiredMaterials do begin
    mRequiredMaterials.Add(item);
  end;

  inherited Create(name, startTime, finishTime, workspace);
end;

destructor TManufactureWorkItem.Free;
begin
   mRequiredMaterials.Free();
end;

function TManufactureWorkItem.getRequiredMaterials: TList<TMaterialRequirement>;
begin
  getRequiredMaterials := mRequiredMaterials;
end;

function TManufactureWorkItem.getType: TWorkspaceItemTypes;
begin
  getType:= ManufactureWork;
end;

{ TWorkspace }

procedure TWorkspace.addItem(item: TWorkspaceItem);
begin
   mItems.Add(item);
end;


function TWorkspace.generateItemID: integer;
begin
   inc(mLastID);
   generateItemID:= mLastID;
end;

function TWorkspace.getItems: TList<TWorkspaceItem>;
begin
  getItems:= mItems;
end;

function TWorkspace.getMaterialInfo(materialName: string): TMaterialInfo;
var material: TMaterialInfo;
begin
  getMaterialInfo:= nil;
  for material in mMaterials do begin
    if material.mName = materialName then begin
      getMaterialInfo:= material;
      break;
    end;
  end;
end;

function TWorkspace.getMaterials: TList<TMaterialInfo>;
begin
  getMaterials:= mMaterials;
end;

procedure TWorkspace.addMaterial(info: TMaterialInfo);
begin
  mMaterials.Add(info);
end;


procedure TWorkspace.clearOrders;
var i: integer;
begin
  i:=0;
  while i < mItems.Count do  begin
    if mItems[i].getType() = MaterialOrder then begin
      removeItem(mItems[i]);
    end
    else inc(i);
  end;
end;

constructor TWorkspace.Create;
begin
  mItems:= TList<TWorkspaceItem>.Create();
  mMaterials:= TList<TMaterialInfo>.Create();
  mLastID:=0;
end;

destructor TWorkspace.Free;
begin
  mItems.Free();
  mMaterials.Free();
end;

procedure TWorkspace.removeItem(item: TWorkspaceItem);
begin
   mItems.Remove(item);
end;

procedure TWorkspace.removeMaterial(info: TMaterialInfo);
begin
  mMaterials.Remove(info);
end;

end.
