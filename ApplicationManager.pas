unit ApplicationManager;

interface

uses Forms, Workspace, MainWindow, MRP, ReportWindow;

type TApplicationManager = class
  public
    constructor Create();

    function run(): integer;
    procedure onRunMrp();

  private
    mWorkspace: TWorkspace;
    mMainWindow: TMainWnd;
    mReportView: TReportView;
    mMrp: TMrp;

end;

implementation

{ TApplicationManager }

constructor TApplicationManager.Create;
begin

end;

procedure TApplicationManager.onRunMrp();
begin
   mWorkspace.clearOrders();
   mMrp.formOrders();
   mMainWindow.syncWithWorkspace();
   mReportView.setReportText(mMrp.generateReport());
   mReportView.show();
end;

function TApplicationManager.run: integer;
begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainWnd, mMainWindow);
  Application.CreateForm(TReportView, mReportView);
  mWorkspace := mMainWindow.Workspace;
  mMrp := TMrp.Create(mWorkspace);
  mMainWindow.OnRunMrpCommand := onRunMrp;
  Application.Run;
end;

end.
