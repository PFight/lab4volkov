object MainWnd: TMainWnd
  Left = 0
  Top = 0
  Caption = 'MainWnd'
  ClientHeight = 589
  ClientWidth = 958
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnMouseWheel = FormMouseWheel
  DesignSize = (
    958
    589)
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonRunMrp: TButton
    Left = 417
    Top = 608
    Width = 107
    Height = 29
    Anchors = [akBottom]
    Caption = 'Run MRP'
    TabOrder = 0
    OnClick = ButtonRunMrpClick
  end
  object Panel1: TPanel
    Left = -153
    Top = 44
    Width = 945
    Height = 559
    Anchors = [akLeft, akTop, akBottom]
    TabOrder = 1
  end
  object ButtonLater: TButton
    Left = 884
    Top = 609
    Width = 75
    Height = 33
    Anchors = [akRight, akBottom]
    Caption = 'Later >>'
    TabOrder = 2
    OnClick = ButtonLaterClick
  end
  object ButtonEralier: TButton
    Left = 0
    Top = 609
    Width = 75
    Height = 33
    Anchors = [akLeft, akBottom]
    Caption = '<< Eralier'
    TabOrder = 3
    OnClick = ButtonEralierClick
  end
  object ButtonAddWork: TButton
    Left = 8
    Top = 8
    Width = 81
    Height = 29
    Caption = 'Add work'
    TabOrder = 4
    OnClick = ButtonAddWorkClick
  end
  object ButtonRemove: TButton
    Left = 182
    Top = 8
    Width = 81
    Height = 29
    Caption = 'Remove'
    TabOrder = 5
    OnClick = ButtonRemoveClick
  end
  object ButtonEdit: TButton
    Left = 95
    Top = 8
    Width = 81
    Height = 29
    Caption = 'Edit'
    TabOrder = 6
    OnClick = ButtonEditClick
  end
  object Button1: TButton
    Left = 280
    Top = 8
    Width = 145
    Height = 29
    Caption = 'Edit materials'
    TabOrder = 7
    OnClick = Button2Click
  end
  object Button2: TButton
    Left = 824
    Top = 8
    Width = 126
    Height = 29
    Anchors = [akTop, akRight]
    Caption = 'Run MRP'
    TabOrder = 8
    OnClick = ButtonRunMrpClick
  end
end
