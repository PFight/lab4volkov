object WorkEditWnd: TWorkEditWnd
  Left = 0
  Top = 0
  Caption = 'Edit work'
  ClientHeight = 337
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    434
    337)
  PixelsPerInch = 96
  TextHeight = 13
  object DateTimePicker2: TDateTimePicker
    Left = 228
    Top = 86
    Width = 186
    Height = 21
    Anchors = [akTop, akRight]
    Date = 41584.890486307870000000
    Time = 41584.890486307870000000
    TabOrder = 0
  end
  object StaticText1: TStaticText
    Left = 214
    Top = 89
    Width = 8
    Height = 17
    Anchors = [akTop]
    Caption = '-'
    TabOrder = 1
  end
  object StaticText2: TStaticText
    Left = 29
    Top = 68
    Width = 30
    Height = 17
    Caption = 'Time:'
    TabOrder = 2
  end
  object ListBoxMaterials: TListBox
    Left = 20
    Top = 137
    Width = 406
    Height = 119
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 3
  end
  object StaticText3: TStaticText
    Left = 29
    Top = 119
    Width = 97
    Height = 17
    Caption = 'Required materials:'
    TabOrder = 4
  end
  object ButtonAddMeterial: TButton
    Left = 20
    Top = 262
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Add'
    TabOrder = 5
    OnClick = ButtonAddMeterialClick
  end
  object ButtonRemoveMaterial: TButton
    Left = 101
    Top = 262
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Remove'
    TabOrder = 6
    OnClick = ButtonRemoveMaterialClick
  end
  object EditName: TEdit
    Left = 20
    Top = 40
    Width = 398
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 7
  end
  object StaticText4: TStaticText
    Left = 29
    Top = 17
    Width = 35
    Height = 17
    Caption = 'Name:'
    TabOrder = 8
  end
  object OK: TButton
    Left = 266
    Top = 303
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 9
    OnClick = OKClick
  end
  object Cancel: TButton
    Left = 347
    Top = 304
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 10
    OnClick = CancelClick
  end
  object DateTimePicker1: TDateTimePicker
    Left = 22
    Top = 85
    Width = 186
    Height = 21
    Date = 41584.890486307870000000
    Time = 41584.890486307870000000
    TabOrder = 11
  end
end
