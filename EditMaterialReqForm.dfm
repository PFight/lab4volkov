object EditMaterialReqWnd: TEditMaterialReqWnd
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Edit material requirement'
  ClientHeight = 122
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ComboBoxMaterial: TComboBox
    Left = 88
    Top = 24
    Width = 169
    Height = 21
    TabOrder = 0
  end
  object SpinEditAmount: TSpinEdit
    Left = 88
    Top = 51
    Width = 65
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 0
  end
  object StaticText1: TStaticText
    Left = 23
    Top = 28
    Width = 46
    Height = 17
    Caption = 'Material:'
    TabOrder = 2
  end
  object StaticText2: TStaticText
    Left = 23
    Top = 55
    Width = 45
    Height = 17
    Caption = 'Amount:'
    TabOrder = 3
  end
  object ButtonEditMaterials: TButton
    Left = 265
    Top = 22
    Width = 89
    Height = 25
    Caption = 'Edit materials'
    TabOrder = 4
    OnClick = ButtonEditMaterialsClick
  end
  object ButtonOK: TButton
    Left = 168
    Top = 86
    Width = 89
    Height = 25
    Caption = 'OK'
    TabOrder = 5
    OnClick = ButtonOKClick
  end
  object ButtonCancel: TButton
    Left = 263
    Top = 86
    Width = 89
    Height = 25
    Caption = 'Cancel'
    TabOrder = 6
    OnClick = ButtonCancelClick
  end
end
